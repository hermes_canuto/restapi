package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"restapi/config"
	"restapi/people"
)

var c = config.Getconfig()
var conn = fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8", c.User, c.Password, c.Host, c.Database)

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	var p people.People
	p.Name = "Priscila"
	p.Email = "Nix@gmail.com"

	db, err := sql.Open("mysql", conn)
	checkErr(err)
	people.DB = db
	defer db.Close()

	i := people.Getpeople(db, 2)
	js, _ := json.Marshal(&i)
	fmt.Println(i.Name)
	fmt.Println(string(js))



	router := mux.NewRouter()

	router.HandleFunc("/people", people.GetPeopleAllEndpoint).Methods("GET")
	router.HandleFunc("/people", people.CreatePersonEndpoint).Methods("POST")
	router.HandleFunc("/people/{id}", people.GetPeopleEndpoint).Methods("GET")



	log.Fatal(http.ListenAndServe(c.Server, router))


}
