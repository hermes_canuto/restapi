package people

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

var DB *sql.DB

type People struct {
	Id    int64  `json:"id,omitempty`
	Name  string `json:"name,omitempty`
	Email string `json:"email,omitempty"`
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func Browse(db *sql.DB) []People {
	rows, err := db.Query("SELECT * FROM tb_people")
	checkErr(err)
	var peoples []People
	for rows.Next() {
		var p People
		err = rows.Scan(&p.Id, &p.Name, &p.Email)
		checkErr(err)
		peoples=append(peoples, p)
	}
	return peoples
}

func Browse2(db *sql.DB) {
	rows, err := db.Query("SELECT * FROM tb_people")
	checkErr(err)
	for rows.Next() {
		var p People
		err = rows.Scan(&p.Id, &p.Name, &p.Email)
		checkErr(err)
		fmt.Println(p.Id, p.Name, p.Email)
	}
}

func Insert(db *sql.DB, people People) int64 {
	sql, err := db.Prepare("INSERT tb_people SET name=?,email=?")
	checkErr(err)
	res, err := sql.Exec(people.Name, people.Email)
	checkErr(err)
	id, err := res.LastInsertId()
	checkErr(err)
	return id
}

func Update(db *sql.DB, id int64, nome string) int64 {
	sql, err := db.Prepare("update userinfo set username=? where uid=?")
	checkErr(err)
	res, err := sql.Exec(nome, id)
	checkErr(err)
	affect, err := res.RowsAffected()
	checkErr(err)
	return affect
}

func Getpeople(db *sql.DB, id int64) People {
	sql, err := db.Prepare("select * from tb_people where id=?")
	checkErr(err)
	var p People
	err = sql.QueryRow(id).Scan(&p.Id, &p.Name, &p.Email)
	if err != nil {
		p.Id = -1
		return p
	}
	return p
}

func CreatePersonEndpoint(w http.ResponseWriter, r *http.Request) {
	var p People
	_ = json.NewDecoder(r.Body).Decode(&p)
	p.Id =Insert(DB,p)
	json.NewEncoder(w).Encode(p)
}

func GetPeopleEndpoint(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, _ := strconv.ParseInt(params["id"], 10, 64)
	people := Getpeople(DB, id)
	fmt.Println("Lendo registro:", people)
	json.NewEncoder(w).Encode(people)
}
func GetPeopleAllEndpoint(w http.ResponseWriter, r *http.Request) {
	people := Browse(DB)
	json.NewEncoder(w).Encode(people)
}